<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupDetail extends Model
{
    protected $fillable = [
        'id', 'payment_term', 'payment_amount', 'cash_out', 'number_members', 'status', 'created_at', 'updated_at',
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];
}
