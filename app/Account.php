<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = [
        'id', 'account_number', 'full_name', 'username', 'password', 'account_type', 'account_code', 'status',
    ];

    protected $hidden = [
        'password',
    ];
}
